const DecrementBtn = ({ decrement }) => {
    return (
        <button
            onClick={decrement}
            style={{ color: 'white', backgroundColor: 'red' }}
        >
            -
        </button>
    )
}

export default DecrementBtn
