import IncrementBtn from './IncrementBtn'
import DecrementBtn from './DecrementBtn'

const Header = ({ number, increment, decrement }) => {
    return (
        <div className="header">
            <IncrementBtn increment={increment} />
            <div>{number}</div>
            <DecrementBtn decrement={decrement} />
        </div>
    )
}

export default Header
