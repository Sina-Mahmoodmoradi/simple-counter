const IncrementBtn = ({ increment }) => {
    return (
        <button
            onClick={increment}
            style={{ color: 'white', backgroundColor: 'green' }}
        >
            +
        </button>
    )
}

export default IncrementBtn
