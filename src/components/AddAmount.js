import { useState } from 'react'

const AddAmount = ({ addAmount }) => {
    const [amount, setAmount] = useState('')

    const onSubmit = (e) => {
        e.preventDefault()
        addAmount(amount)

        setAmount('')
    }
    return (
        <div className="AddAmount">
            <form onSubmit={onSubmit}>
                <input
                    type="number"
                    value={amount}
                    onChange={(e) => setAmount(Number(e.target.value))}
                    style={{ width: '50px' }}
                />
                <input type="submit" value="Add Amount" />
            </form>
        </div>
    )
}

export default AddAmount
