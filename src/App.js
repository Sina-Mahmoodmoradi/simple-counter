import Header from './components/Header'
import AddAmount from './components/AddAmount'
import { useState } from 'react'

function App() {
    const [number, setNumber] = useState(0)

    const increment = () => {
        setNumber(number + 1)
    }

    const decrement = () => {
        setNumber(number - 1)
    }

    const addAmount = (amount) => {
        setNumber(number + amount)
    }

    return (
        <div className="container">
            <Header
                number={number}
                increment={increment}
                decrement={decrement}
            />
            <AddAmount addAmount={addAmount} />
        </div>
    )
}

export default App
